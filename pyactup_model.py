import gc
import pyactup
import pyibl
import random
from timeit import timeit

PARTICIPANTS = 100
ROUNDS = 200
ITERATIONS = 10
TEMPERATURE = 1
MISMATCH = 10
DEFAULT_UTILITY = 12

def outcome(button, n):
    if button == "A":
        return 5
    elif random.random() < 1 - n / 100:
        return 0
    else:
        return 10

pyactup.set_similarity_function(pyibl.bounded_linear_similarity(0, 100), "n")

def run():
    mem = pyactup.Memory(temperature=TEMPERATURE, mismatch=MISMATCH)
    average = 0
    for p in range(PARTICIPANTS):
        mem.reset()
        for b in "AB":
            for n in (0, 100):
                mem.learn(button=b, n=n, outcome=DEFAULT_UTILITY, advance=0)
        mem.advance()
        score = 0
        for r in range(ROUNDS):
            n = random.randint(0, 100)
            choice, bv = mem.best_blend("outcome", [{"button": "A", "n": n},
                                                    {"button": "B", "n": n}])
            result = outcome(choice["button"], n)
            score += result
            choice["outcome"] = result
            mem.learn(**choice)
        average += score
    return average / PARTICIPANTS

def main():
    gc.collect()                # pointless ritual
    for i in range(ITERATIONS):
        print(timeit(stmt="run()", globals=globals(), number=1))

if __name__ == "__main__":
    main()
