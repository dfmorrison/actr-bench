import gc
import pyactup
import pyibl
import random
from timeit import timeit

PARTICIPANTS = 100
ROUNDS = 200
ITERATIONS = 10
TEMPERATURE = 1
MISMATCH_PENALTY = 10
DEFAULT_UTILITY = 12

def outcome(button, n):
    if button == "A":
        return 5
    elif random.random() < 1 - n / 100:
        return 0
    else:
        return 10

pyibl.similarity(pyibl.bounded_linear_similarity(0, 100), "n")

def run():
    agent = pyibl.Agent(attributes=["button", "n"],
                        temperature=TEMPERATURE,
                        mismatch_penalty=MISMATCH_PENALTY)
    average = 0
    for p in range(PARTICIPANTS):
        agent.reset()
        agent.populate(DEFAULT_UTILITY, ("A", 0), ("A", 100), ("B", 0), ("B", 100))
        score = 0
        for r in range(ROUNDS):
            n = random.randint(0, 100)
            choice = agent.choose(("A", n), ("B", n))
            result = outcome(choice[0], n)
            score += result
            agent.respond(result)
        average += score
    return average / PARTICIPANTS

def main():
    gc.collect()                # pointless ritual
    for i in range(ITERATIONS):
        print(timeit(stmt="run()", globals=globals(), number=1))

if __name__ == "__main__":
    main()
