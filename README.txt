This is a small, artificial benchmark comparing different implementations of ACT-R declarative memory. So far only Lisp ACT-UP, PyACTUp and PyIBL versions are implemented, but I hope to soon add versions in ACT-R 7.x, ACT-R Classic, and ACT-R 7.x run via Python.

==========

The task is an iterated, binary choice game. From the player’s perspective at each round it is presented an integer value, n, between 0 and 100, inclusive, asked to make a choice between “A” or “B,” and then given a numeric result, one of the integers 0, 5 or 10. The goal is to maximize this result, based on experience.

The model used is a simple IBL model choosing between “A” and “B,” with an additional slot, n. Partial matching is used with a linear similarity function on n. Prepopulated instances are added, all with a result of 12, for (A, 0), (A, 100), (B, 0) and (B, 100). The game is played for 200 rounds, by 100 independent, virtual participants. A mismatch penalty of 10, noise 0.25, decay 0.5 and blending temperature 1 were used.

Under the covers the result for a choice is computed as:

• A is a “safe” option always resulting in 5 points.

• B is a “risky” option, sometimes resulting in 0 and sometimes in 10 points. Which is decided randomly with probability 1 - n/100 that it will be 0, and otherwise 10. At each round the integer value n is chosen uniformly randomly from the range [0, 100].

Note choosing A or B randomly, or equivalently always choosing A or always choosing B, can be expected to result in about 1,000 points.

With knowledge of the payoff structure we can see that optimum play is to pick A if n < 50, and to pick B if n > 50 (if n = 50 it doesn’t matter which we pick). With this optimum strategy we can expect to get around 1,250 points, though, of course, the result is stochastic.

The IBL model earns around 1,200 points. Again stochastic.

==========

The benchmarks were run on a machine with a 4 GHz Intel Core i7-6700K CPU and 32 GB of 2.13 GHz memory, running Ubuntu 20.04. Each was run 10 times (that is, ten different times, each time running 100 participants), and the fastest of the ten is what is reported.

For the Python versions Python 3.9.0 was used. For the Lisp versions SBCL 2.0.9 and CCL 1.11 were used. PyIBL version 4.1.1, PyACTUp 1.0.8 and Lisp ACT-UP v1_2 were used.

Results:

PyIBL:       13.847 seconds

PyACTUp:     13.617 seconds

CCL ACT-UP:   3.039 seconds

SBCL ACT-UP:  0.811 seconds

==========

Caveats:

• Benchmarks measure the speeds of the particular benchmarks. Real code can differ markedly.

• It’s hard to tease apart how much the differences are due to Python versus Lisp, and how much to the different implementations of ACT-R DM. For example, I believe the Lisp versions are actually computing roughly twice as many activations as the Python ones, so actually have a disadvantage — if this were not the case they might be winning by even wider margins.

• The benchmark is stochastic, so different sequences of pseudo-random numbers could result in different execution times. That said, I don’t think I’ve observed runs differing by even as much as 1%.

• Little effort has been made to tune things. Maybe they could be made faster.

• Speed isn’t everything.

==========

Addendum:

Thinking further about the issue of the Lisp ACT-UP version running its inner look roughly twice as many times as really necessary, it occurred to me that a slighly more complex model, using two memory objects, one for A and one for B, could obviate this. Such a benchmark is now in alternative-actup-model.lisp. Turns out it more than doubles the speed of the model.

Results:

CCL alternative ACT-UP:   1.381 seconds

SBCL alternative ACT-UP:  0.332 seconds
