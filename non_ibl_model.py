import random

PARTICIPANTS = 10_000
ROUNDS = 200
ITERATIONS = 10

def outcome(button, n):
    if button == "A":
        return 5
    elif random.random() < 1 - n / 100:
        return 0
    else:
        return 10

def run(smart=True):
    average = 0
    for p in range(PARTICIPANTS):
        score = 0
        for r in range(ROUNDS):
            n = random.randint(0, 100)
            if smart and n < 50:
                button = "A"
            elif smart and n > 50:
                button = "B"
            else:
                button = random.choice("AB")
            score += outcome(button, n)
        average += score
    return average / PARTICIPANTS

def main():
    print("dumb")
    for i in range(ITERATIONS):
        print(run(False))
    print("\nsmart")
    for i in range(ITERATIONS):
        print(run(True))


if __name__ == "__main__":
    main()
